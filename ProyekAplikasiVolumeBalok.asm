include 'emu8086.inc'
#make_com#
org 100h
jmp mulai
    bar1 db 13,10,13,10,"Menghitung Volume Balok",13,10,13,10,'$'
    bar2 db "Masukkan Panjang : $"
    bar3 db "Masukkan Lebar : $"
    bar4 db "Masukkan Tinggi : $"
    bar5 db "Volume Balok Adalah : $"
    bar6 db 13,10,13,10,"Apakah Ingin Mengulangi Program [y/t]: $"
    bar7 db 13,10,"Program Selesi $"  
    
    
    panjang dw ?
    lebar   dw ?
    tinggi  dw ?
    hasil   dw ?
    
mulai:
    lea dx, bar1
    mov ah, 09h
    int 21h
    lea dx, bar2
    mov ah, 09h
    int 21h
    call scan_num
    mov panjang, cx
    
    putc 13
    putc 10  
    
    lea dx, bar3
    mov ah, 09h
    int 21h
    call scan_num
    mov lebar, cx
    
    putc 13
    putc 10
    
    lea dx, bar4
    mov ah, 09
    int 21h
    call scan_num
    mov tinggi, cx
    
    mov ax, panjang
    mov bx, lebar
    mov cx, tinggi
    imul bx
    imul cx    
    push ax
    
    putc 13
    putc 10
    
    lea dx, bar5
    mov ah, 09h
    int 21h
    
    pop ax
    mov hasil, ax
    call print_num
    
    lea dx, bar6
    mov ah, 09h
    int 21h
    mov ah, 01h
    int 21h
    cmp al, 'y'
    je mulai
    cmp al, 't'
    je exit
    
    
exit:
    lea dx, bar7 
    mov ah, 09h
    int 21h
ret  
    define_scan_num
    define_print_num
    define_print_num_uns
end
  